#!/usr/bin/env node

const { getOrgAlias, deleteOrg } = require('./force');

const run = async () => {
    const alias = await getOrgAlias();

    if (alias) {
        await deleteOrg(alias);
    }
};

run();
