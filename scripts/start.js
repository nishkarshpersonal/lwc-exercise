#!/usr/bin/env node

const {
    createOrg,
    pushSource,
    importData,
    enableDebugMode,
    setDefaultOrg,
    openOrg
} = require('./force');

const { basename } = require('path');

const run = async alias => {
    const username = await createOrg(alias, 'config/project-scratch-def.json');

    if (!username) {
        return;
    }

    await setDefaultOrg(alias);

    await pushSource(alias);
    await importData(alias, 'data/SalesgramPost-data.json');
    await enableDebugMode(username);
    await openOrg(alias);
};

run(basename(process.env.PWD));
