const { exec } = require('child_process');

const ansi = {
    reset: '\x1b[0m',
    bright: '\x1b[1m',
    dim: '\x1b[2m',
    underscore: '\x1b[4m',
    red: '\x1b[31m',
    green: '\x1b[32m',
    blue: '\x1b[34m',
    cyan: '\x1b[36m',
    magenta: '\x1b[35m',
    yellow: '\x1b[33m'
};

const log = msg => console.log(msg);

const err = message => {
    log(`${ansi.red}${message}${ansi.reset}`);
};

const cmd = async command => {
    return new Promise((resolve, reject) => {
        exec(command, (err, stdout, stderr) => {
            if (err) {
                reject(stderr);
            } else {
                resolve(stdout);
            }
        });
    });
};

const sfdx = async (...args) => {
    try {
        return JSON.parse(await cmd(`sfdx ${args.join(' ')} --json`)).result;
    } catch (e) {
        err(JSON.parse(e).message);
        return null;
    }
};

module.exports = {
    ansi,
    log,
    err,
    cmd,
    sfdx
};
