const { log, ansi, sfdx } = require('./utils');

const createOrg = async (alias, configFile) => {
    log(
        `${ansi.green}Creating a scratch org with alias ${ansi.cyan}${alias}${ansi.green}...${ansi.reset}`
    );

    const org = await sfdx(
        `force:org:create`,
        `-f ${configFile}`,
        `-a ${alias}`,
        `-d 7`,
        `-w 60`,
        `-s`
    );

    if (org) {
        const { username } = org;
        log(
            `${ansi.green}Org username: ${ansi.cyan}${username}${ansi.reset}\n`
        );
        return username;
    }
};

const deleteOrg = async alias => {
    log(
        `${ansi.green}Deleting the scratch org with alias ${ansi.cyan}${alias}${ansi.green}...${ansi.reset}`
    );

    await sfdx(`force:org:delete -u ${alias} -p`);
};

const setDefaultOrg = async alias => {
    log(
        `${ansi.green}Setting the scratch org with alias ${ansi.cyan}${alias}${ansi.green} as default...${ansi.reset}\n`
    );

    await sfdx(`force:config:set defaultusername=${alias}`);
};

const openOrg = async alias => {
    log(`${ansi.green}Opening the scratch org...${ansi.reset}`);
    await sfdx(`force:org:open -u ${alias} -p one/one.app`);
};

const getOrgAlias = async () => {
    log(`${ansi.green}Reading org information...${ansi.reset}`);

    const list = await sfdx(`force:org:list`);

    if (!list) {
        return;
    }

    const org = (list.scratchOrgs || []).filter(
        org => org.defaultMarker === '(U)'
    )[0];

    if (!org) {
        log(`${ansi.red}No default scratch org set.${ansi.reset}`);
        return;
    }

    return org.alias;
};

const pushSource = async alias => {
    log(`${ansi.green}Pushing source code...${ansi.reset}`);
    await sfdx(`force:source:push -u ${alias} -f`);
};

const importData = async (alias, dataFile) => {
    log(`${ansi.green}Loading sample data...${ansi.reset}`);
    await sfdx(`force:data:tree:import`, `-u ${alias}`, `-f ${dataFile}`);
};

const enableDebugMode = async username => {
    log(`${ansi.green}Enabling debug mode...${ansi.reset}`);
    await sfdx(
        `force:data:record:update`,
        `-s User`,
        `-w "username='${username}'"`,
        `-v "UserPreferencesUserDebugModePref='true'"`
    );
};

module.exports = {
    createOrg,
    deleteOrg,
    setDefaultOrg,
    openOrg,
    getOrgAlias,
    pushSource,
    importData,
    enableDebugMode
};
